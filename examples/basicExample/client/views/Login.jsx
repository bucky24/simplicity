import { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

import { UserContext } from "@bucky24/simplicity/client";

export default function Login() {
    const { error, login, loggedIn } = useContext(UserContext);
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const navigate = useNavigate();

    useEffect(() => {
        if (loggedIn) {
            navigate('/');
        }
    }, [loggedIn]);

    return <div>
        <h2>Login</h2>
        {error && <div>{error}</div>}
        <form onSubmit={(e) => {
            e.preventDefault();
            login(username, password);
        }}>
            <div>
                <label htmlFor="username">Username</label>
                <input value={username} onChange={(e) => setUsername(e.target.value)}id="username" type="text" />
            </div>
            <div>
                <label htmlFor="password">Password</label>
                <input value={password} onChange={(e) => setPassword(e.target.value)}id="password" type="password" />
            </div>
            <button>Login</button>
            <button onClick={() => {
                navigate('/register');
            }}>Register</button>
        </form>
    </div>
}