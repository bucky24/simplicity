import { useContext, useEffect, useState } from "react";
import { useNavigate } from 'react-router-dom';

import { UserContext } from "@bucky24/simplicity/client";

export default function Home() {
    const { loggedIn, callApi, callAuthedApi, logout } = useContext(UserContext);
    const navigate = useNavigate();
    const [messages, setMessages] = useState([]);
    const [newMessage, setNewMessage] = useState('');

    const getMessages = () => {
        callApi('/api/messages').then((response) => {
            setMessages(response);
        });
    };

    useEffect(() => {
        getMessages();
    }, []);

    return <div>
        <h2>Home</h2>
        {!loggedIn && <button onClick={() => {
            navigate('/login');
        }}>Login</button>}
        {loggedIn && <button onClick={() => {
            logout();
        }}>Logout</button>}
        <h2>Messages</h2>
        {messages.map((message) => {
            return <div key={message.id}>
                {message.message} from {message.user_id}
            </div>
        })}
        {loggedIn && <div>
            <h3>New Message</h3>
            <input type="text" value={newMessage} onChange={(e) => setNewMessage(e.target.value)} />
            <button onClick={() => {
                callAuthedApi('/api/messages', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify({ message: newMessage }),
                }).then(() => {
                    setNewMessage('');
                    getMessages();
                });
            }}>New Message</button>
        </div>}
    </div>
}