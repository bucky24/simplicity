const { init, createModel, TYPES, Api, AuthedApi } = require('../../../index');

const messageModel = createModel('messages', {
    user_id: TYPES.INTEGER,
    message: TYPES.STRING,
});

Api.get('/messages', async (req, res) => {
    const messages = await messageModel.search();

    res.status(200).json(messages);
});

AuthedApi.post('/messages', async (req, res) => {
    const { message } = req.body;

    const messageObj = await messageModel.insert({
        user_id: req.user.id,
        message,
    });

    res.status(200).json(messageObj);
});

init(__dirname, {
    projectName: 'Basic Example',
});