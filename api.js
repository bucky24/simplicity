const { Auth } = require('@bucky24/toolbox/server');
const { Router } = require('express');
const { UserModel } = require('./models');
const bcrypt = require('bcrypt');

const apiData = {
    api: [],
    authedApi: [],
};

const Api = {
    get: (path, callback) => {
        apiData.api.push({ method: 'get', path, callback });
    },
    delete: (path, callback) => {
        apiData.api.push({ method: 'delete', path, callback });
    },
    post: (path, callback) => {
        apiData.api.push({ method: 'post', path, callback });
    },
    put: (path, callback) => {
        apiData.api.push({ method: 'put', path, callback });
    },
    patch: (path, callback) => {
        apiData.api.push({ method: 'patch', path, callback });
    },
}

const AuthedApi = {
    get: (path, callback) => {
        apiData.authedApi.push({ method: 'get', path, callback });
    },
    delete: (path, callback) => {
       apiData.authedApi.push({ method: 'delete', path, callback });
    },
    post: (path, callback) => {
        apiData.authedApi.push({ method: 'post', path, callback });
    },
    put: (path, callback) => {
        apiData.authedApi.push({ method: 'put', path, callback });
    },
    patch: (path, callback) => {
       apiData.authedApi.push({ method: 'patch', path, callback });
    },
}

function createApiObjects(app) {
    const router = new Router();

    for (const api of apiData.api) {
        router[api.method](api.path, api.callback);
    }
    
    for (const authedApi of apiData.authedApi) {
        router[authedApi.method](authedApi.path, (req, res, next) => {
            Auth.checkAuth(req, res, async () => {
                const id = req.authData.id;
                const user = await UserModel.search({ id });
                if (user.length === 0) {
                    console.log(`No user found with id ${id}`);
                    res.status(401).end();
                    return;
                }
                req.user = user[0];
                next();
            });
        }, authedApi.callback);
    }

    router.post('/user', async (req, res) => {
        const { username, password } = req.body;

        const hashed = await bcrypt.hash(password, 10);

        const userId = await UserModel.insert({
            username,
            password: hashed,
        });

        Auth.createToken(res, { id: userId });
    });

    router.post('/user/login', async (req, res) => {
        const { username, password } = req.body;

        const users = await UserModel.search({
            username,
        });

        if (users.length === 0) {
            res.status(401).end();
            return;
        }

        const user = users[0];

        const isMatch = await bcrypt.compare(password, user.password);

        if (!isMatch) {
            res.status(401).end();
            return;
        }

        Auth.createToken(res, { id: user.id });
    });

    app.use('/api', router);
}

module.exports = {
    createApiObjects,
    Api,
    AuthedApi,
};