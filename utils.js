const path = require("path");
const fs = require("fs");

function createProjectFiles(projectPath, options) {
    const projectName = options.name || 'My Project';

    const clientDir = path.resolve(projectPath, "..", "client");
    const myClientDir = path.resolve(projectPath, "..", "build");
    if (fs.existsSync(myClientDir)) {
        fs.rmSync(myClientDir, { recursive: true });
    }
    fs.mkdirSync(myClientDir);

    const importDir = path.relative(myClientDir, clientDir);
    const indexFile = `import { createRoot } from 'react-dom/client';
import App from '${importDir}/App';
import { UserProvider } from '@bucky24/simplicity/client';

createRoot(document.getElementById('root')).render(
    <UserProvider>
        <App />
    </UserProvider>
)`;
    fs.writeFileSync(path.join(myClientDir, "index.jsx"), indexFile);

    const indexHtml = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>${projectName}</title>
</head>
<body>
    <div id="root"></div>
    <script type="module" src="/index.jsx"></script>
</body>
</html>`;
    fs.writeFileSync(path.join(myClientDir, "index.html"), indexHtml);

    return {
        clientDir,
        myClientDir,
    };
}

module.exports = {
    createProjectFiles,
};