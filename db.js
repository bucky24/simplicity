const { Connection } = require('@bucky24/database');
const { models } = require('./models');

function getDatabaseCreds() {
    if (process.env.DB_HOST) {
        return {
            type: "mysql",
            creds: {
                host: process.env.DB_HOST,
                username: process.env.DB_USER,
                password: process.env.DB_PASS,
                database: process.env.DB_DB,
            }
        }
    } else {
        throw new Error("Can't get database credentials");
    }
}

async function startDatabase() {
    const { type, creds } = getDatabaseCreds();
    if (type === "mysql") {
        try {
            const connection = await Connection.mysqlConnection(creds);
            Connection.setDefaultConnection(connection);
        } catch (e) {
            console.log(e.message);
            if (e.message.includes("does not exist")) {
                console.log(`Database doesn't exist. Please create it with\n\nCREATE DATABASE ${creds.database}\n\n`);
                process.exit(1);
            } else {
                throw e;
            }
        }
    } else {
        throw new Error("Unknown database type", type);
    }

    if (models.length > 0) {
        await models[0].init();
    }

    if (models.length > 1) {
        await Promise.all(models.slice(1).map(model => model.init()));
    }

    console.log(`Connected to database successfully`);
}

module.exports = {
    getDatabaseCreds,
    startDatabase,
};