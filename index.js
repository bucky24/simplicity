const express = require("express");
const path = require("path");
const fs = require("fs");
const { createServer } = require("vite");
const react = require('@vitejs/plugin-react');
const cookieParser = require("cookie-parser");

const { startDatabase } = require("./db");
const { createModel, TYPES, UserModel } = require("./models");
const { createApiObjects, Api, AuthedApi } = require("./api");
const { createProjectFiles } = require("./utils");

const port = process.env.PORT || 3001;

async function init(projectPath, options = {}) {
    const isProduction = process.env.NODE_ENV === "production";
    require("dotenv").config({ path: path.join(projectPath, "..", ".env") });
    const app = express();

    const clientDir = path.resolve(projectPath, "..", "client");

    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));
    if (isProduction) {
        console.log(path.join(projectPath, "..", "dist"));
        app.use(express.static(path.join(projectPath, "..", "dist")));
    }
    app.use(cookieParser());

    const apis = createApiObjects(app);

    await startDatabase()
    app.listen(port, () => {
        console.log(`Listening on port ${port}`);
    });

    if (!isProduction) {
        const { myClientDir } = createProjectFiles(projectPath, options);

        const server = await createServer({
            mode: 'development',
            plugins: [react()],
            configFile: false,
            root: myClientDir,
            server: {
            port: 8080,
            proxy: {
                '^/api/.*': {
                target: `http://localhost:${port}`,
                changeOrigin: true,
                secure: false, 
                }
            },
            },
            resolve: {
                alias: {
                    '@bucky24/simplicity/client': path.join(__dirname, "client.js"),
                },
            },
        });

        await server.listen();
        server.printUrls();
    }
}

module.exports = {
    init,
    createModel,
    TYPES,
    UserModel,
    AuthedApi,
    Api,
}