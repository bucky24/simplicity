import React, { useEffect, useState } from 'react';

const UserContext = React.createContext({});
export default UserContext;

class ApiError extends Error {
    constructor(message, status) {
        super(message);
        this.status = status;
        this.name = 'ApiError';
    }
}

export function UserProvider({ children }) {
    const [loggedIn, setLoggedIn] = useState(false);
    const [error, setError] = useState(null);
    const [token, setToken] = useState(null);

    useEffect(() => {
        const token = localStorage.getItem('token');
        setToken(token);
        if (token) {
            setLoggedIn(true);
        }
    }, []);

    useEffect(() => {
        if (token) {
            localStorage.setItem('token', token);
        }
    }, [token]);

    const logout = () => {
        setToken(null);
        setLoggedIn(false);
        localStorage.removeItem('token');
    };

    const callApi = async (url, options) => {
        const result = await fetch(url, options);
        
        if (!result.ok) {
            const error = await result.text();
            console.error(`Could not call api ${url}: ${error} (status ${result.status})`);
            throw new ApiError(error, result.status);
        }

        const jsonResult = await result.json();

        return jsonResult;
    };

    const callAuthedApi = async (url, options) => {
        if (!token) {
            throw new Error("Cannot call authed api without token");
        }

        try {
            const result = await callApi(url, {
                ...options,
                headers: {
                    ...options.headers,
                    Authorization: `Bearer ${token}`,
                }
            });
            return result;
        } catch (error) {
            if (error instanceof ApiError) {
                if (error.status === 401) {
                    logout();
                }
            } else {
                console.error(error);
            }
        }
    };

    const value = {
        loggedIn,
        error,
        callApi,
        callAuthedApi,
        login: async (username, password) => {
            setLoggedIn(false);
            setError(null);
            try {
                const token = await callApi('/api/user/login', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify({ username, password }),
                });
                setLoggedIn(true);
                setToken(token);
            } catch (error) {
                setError("Unable to login");
            }
        },
        createUser: async (username, password) => {
            try {
                const token = await callApi('/api/user', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify({ username, password }),
                });

                setLoggedIn(true);
                setToken(token);
            } catch (error) {
                setError("Unable to register");
            }
        },
        logout
    };

    return <UserContext.Provider value={value}>
        {children}
    </UserContext.Provider>
}