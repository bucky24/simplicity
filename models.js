const { Model, FIELD_TYPE } = require('@bucky24/database');

const models = [];

const TYPES = {
    STRING: FIELD_TYPE.STRING,
    INTEGER: FIELD_TYPE.INT,
};

function createModel(tableName, data, version=1) {
    const model = Model.create({
        table: tableName,
        fields: Object.keys(data).reduce((fields, key) => {
            const type = data[key];
            return {
                ...fields,
                [key]: {
                    type,
                },
            };
        }, {}),
        version,
    });

    models.push(model);

    return model;
}

const UserModel = createModel('users', {
    username: TYPES.STRING,
    password: TYPES.STRING,
});

module.exports = {
    createModel,
    TYPES,
    models,
    UserModel,
};