# Simplicity

## What Is It?

The idea behind Simplicity is that often people just want to start building something, and don't want to spend a ton of time writing boilerplate code.

If you don't want to have to think about what libraries you're using, how to setup React, MySQL, etc, then Simplicity may help. It provides an extremely simple interface that is intended to give you the fewest amount of decisions necessary to get your server off the ground and running.

## Usage

### Installation

*Note*: Simplicity requires you to have react and react-dom installed.

To install Simplicity, simply install with npm:

    npm install @bucky24/simplicity react react-dom

Or yarn:

    yarn add @bucky24/simplicity react react-dom

Or pnpm:

    pnpm install @bucky24/simplicity react-react-dom

### Quick Start Example

#### Folder Structure

Simplicity expects your project to have a specific folder structure, and will not be able to work otherwise.

```
project_root ->
    client ->
        App.jsx
    src ->
        server.js
    .env
```

Simplicity will also create `project_root/build` and will generate `index.html` and `index.jsx` files in that folder. It is recommended that you add this folder to your `.gitignore`.

#### Code

##### src/server.js
```js
const { init, createModel, TYPES, Api } = require('@bucky24/simplicity');

// create a database table
const messageModel = createModel('messages', {
    user_id: TYPES.INTEGER,
    message: TYPES.STRING,
});

// setup an api
Api.get('/messages', async (req, res) => {
    const messages = await messageModel.search();

    res.status(200).json(messages);
});

// start the server
init(__dirname, {
    projectName: "my cool project",
});
```

##### client/App.jsx

```js
export default function App() {
    return <div>Welcome to Simplicity!</div>;
}
```

##### .env
```
DB_HOST=localhost
DB_USER=my_user
DB_PASS=my_password
DB_DB=my_project_database
JWT_KEY=secret_key_for_jwt
```

#### Commands

To run the server, simply run `node --watch ./src/server.js`.

To build your code for production, the `simplicity build` command can be used.

When `NODE_ENV` is set to `production`, the Simplicity server will use `express.static` to load the `dist` folder by default.

### Auth

Simplicity handles auth for you. It uses JWT tokens and provides mechanisms for login, logout, and storing user information.

#### Backend

On the backend, the only question you need to ask yourself is "Does this API need authentication or not?".

If you find your API does not need authentication, you can use the `Api` object as detailed below. However, if you do want authentication, you can make use of the `AuthedApi` object. The only difference as far as your code is concerned is that `AuthedApi` methods may return a 401, and will set the `req.user` object, which is a copy of the object that called the API.

#### Frontend

On the frontend, you can make use of the `UserContext` to determine if the user is currently logged in, as well as take actions like logging in and out. You can also use `UserContext` to perform an authenticated API request to the server (details below).

### Database

Simplicity uses an ORM connecting to a MySQL database. It takes care of all the connection and table creation/maintaining on your behalf.

In order to connect, Simplicity requires the following fields to be defined in your `.env`:

| Field | Description |
| -- | -- |
| DB_HOST | The host for your database server |
| DB_USER | The username for your server |
| DB_PASS | The password for your user |
| DB_DB | The actual database to connect to (must already exist) |

### Web Server

Simplicity uses `express` as its web server. All common middleware is already enabled on the app, so you can just focus on creating APIs.

### Frontend Building

Simplicity has `vite` compiling built in. All you have to do is start your server.