#!/usr/bin/env node

const [,,...args] = process.argv;

if (args.length === 0) {
    console.log("Please specify a command");
    process.exit(1);
}

const command = args[0];

if (command === "build") {
    const { build } = require("vite");
    const path = require("path");
    const fs = require("fs");
    const react = require('@vitejs/plugin-react');

    const buildDir = path.resolve(process.cwd(), "build");

    if (!fs.existsSync(buildDir)) {
        console.log("Build directory does not exist, please run your server.js at least once to pre-build files");
        process.exit(1);
    }

    build({
        root: path.resolve(process.cwd(), "build"),
        plugins: [react()],
        mode: "production",
    }).then(() => {
        // now dist is inside the build directory
        fs.rmSync(path.resolve(process.cwd(), "dist"), { recursive: true });
        fs.renameSync(path.resolve(process.cwd(), "build", "dist"), path.resolve(process.cwd(), "dist"));
    });
} else {
    console.log(`Unknown command ${command}`);
    process.exit(1);
}